<?php
	require_once("action/MainMenuAction.php");

	$action = new MainMenuAction();
	$action->execute();

	require_once("partial/header.php");
?>

<script src="js/mainmenu.js"></script>
<script src="js/utils.js"></script>
<div class="mainmenu" style="display:none;">
	<div class="mainmenu_title">SOCCER CARAMBOLE</div>
	<div id="mainmenu_welcom"></div>
	<button id="btnLibre">LIBRE</button>
	<button id="btn_1Bande">1 BANDE</button>
	<button id="btn_3Bandes">3 BANDES</button>
	<div>
		<a href="?logout=true">Logout</a>
	</div>
</div>

<?php
	require_once("partial/footer.php");