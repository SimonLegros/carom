<?php
	require_once("action/AjaxQuitGameAction.php");

	$action = new AjaxQuitGameAction();
	$action->execute();

	echo json_encode($action->result);