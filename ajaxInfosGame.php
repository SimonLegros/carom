<?php
	require_once("action/AjaxInfosGameAction.php");

	$action = new AjaxInfosGameAction();
	$action->execute();

	echo json_encode($action->result);