<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	require_once("partial/header.php");
?>
	<script src="js/index.js"></script>
	<div class="login-box" id="tabs">
		<ul>
			<li><a href="#tab-login">LOG IN</a></li>
			<li><a href="#tab-signin">SIGN UP</a></li>
		</ul>
		<div id="tab-login">
			<form action="index.php?action=login" method="post">
				<div>Nom d'usager</div>
				<div><input required type="username" name="username-login" autocomplete="off" value="<?= $action->lastUsername ?>"/></div>
				<div>Mot de passe</div>
				<div><input required type="password" name="password-login" /></div>
				<div class="btnCtn">
					<button type="submit" id="btnLogin">Log in</button>
				</div>
			</form>
		</div>
		<div id="tab-signin">
			<form action="index.php?action=signin" method="post">
				<div>Nom d'usager</div>
				<div><input required type="username" name="username-signin" autocomplete="off"/></div>
				<div>Mot de passe</div>
				<div><input required type="password" name="password-signin" /></div>
				<div class="btnCtn">
					<button type="submit" id="btnSignin">Sign in</button>
				</div>
			</form>
		</div>
		<?php 
			if ($action->wrongLogin) {
				?>
				<div class="error-div"><?= $action->status ?></div>
				<?php
			}
		?>
	</div>
<?php
	require_once("partial/footer.php");