<?php
	require_once("action/AjaxChooseGameAction.php");

	$action = new AjaxChooseGameAction();
	$action->execute();

	echo json_encode($action->result);