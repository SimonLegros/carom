<?php
	require_once("action/AjaxAngleJoueurAction.php");

	$action = new AjaxAngleJoueurAction();
	$action->execute();

	echo json_encode($action->result);
