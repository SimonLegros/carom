<?php
	require_once("action/AjaxAddAction.php");

	$action = new AjaxAddAction();
	$action->execute();

	echo json_encode($action->result);
