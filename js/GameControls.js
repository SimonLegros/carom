let currentAngle = 0;
let currentForce = 25;

class GameControls{
	constructor(){
		this.mainPanel = null;
		this.slider_angle = null;
		this.slider_force = null;
		this.btnHit = null;
		this.currentAngle = currentAngle;
		this.currentForce = currentForce;
		this.createPanel();
		this.addListeners();
		$("#slider_angle").roundSlider({
			min: 0,
    		max: 360,
    		step: 1,
			value: 0,
			radius: 80,
			width: 8,
			handleSize: "+16",
			handleShape: "dot",
			sliderType: "min-range",
    		drag: "sliderAngleEvent"
		});
		let handle = $( "#custom-handle" );
		$( "#slider_force" ).slider({
			value: currentForce,
			min:1,
			create: function() {
				handle.text( $( this ).slider( "value" ) );
			},
			slide: function( event, ui ) {
				handle.text( ui.value );
				currentForce = ui.value;
				view.movePlayerStick(currentAngle, currentForce);
			}
		});
		// setInterval(sendPlayerStick, 1000);
	}

	createPanel(){
		this.mainPanel = document.createElement("div");
		this.mainPanel.className = "panelControls";

		this.slider_angle = document.createElement("div");
		this.slider_angle.id = "slider_angle";
		this.slider_angle.style.margin = "10px";
		this.mainPanel.appendChild(this.slider_angle);

		this.slider_force = document.createElement("div");
		this.slider_force.id = "slider_force";
		this.slider_force.style.margin = "10px";
		let handle = document.createElement("div");
		handle.id = "custom-handle";
		handle.className = "ui-slider-handle";
		this.slider_force.appendChild(handle);
		this.mainPanel.appendChild(this.slider_force);

		this.btnHit = document.createElement("button");
		this.btnHit.style.margin = "10px";
		this.btnHit.textContent = "HIT !";
		this.mainPanel.appendChild(this.btnHit);

		document.body.appendChild(this.mainPanel);
	}

	addListeners(){
		this.btnHit.onclick = (e) =>{
			// let data = {
			// 	currentForce: currentForce,
			// 	currentAngle: currentAngle
			//   };
			// let event = new CustomEvent('hitBall', { detail: data });
			// window.dispatchEvent(event);
			this.hide();
			// view.hidePlayerStick();
			view.playStickAnimation(currentAngle, currentForce);

		}
	}

	show(){
		$(".panelControls").fadeIn();
	}

	hide(){
		$(".panelControls").fadeOut();
	}

}
function sliderAngleEvent(e){
	currentAngle = e.value;
	view.movePlayerStick(currentAngle, currentForce);
}

function sendPlayerStick(){
	formData = new FormData();
	formData.append("angle", currentAngle);
	formData.append("distance", currentForce);
	formData.append("idPartie", localStorage.getItem("id-partie"));
	fetch('ajaxAngleJoueur.php', {
		method: "POST",
		credentials: "include",
		body: formData
	})
	.then(function(reponse){
		reponse.text().then(function(data){
		})
	})
}