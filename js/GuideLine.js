class GuideLine {
	constructor(obj){
		this.startX = null;
		this.startY = null;
		this.endX = null;
		this.endY = null;
		this.width = 10;
		this.visible = false;
		this.obj = obj;
	}

	setPos(sX, sY, eX, eY){
		this.startX = sX;
		this.startY = sY;
		this.endX = eX;
		this.endY = eY;
	}

	update(){

	}

	setVisible(visible=true){
		this.visible = visible;
	}
}