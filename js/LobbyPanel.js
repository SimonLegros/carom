class LobbyPanel{
	constructor(){
		this.mainpanel = null;
		this.panelPlayers = null;
		this.loadingSprite = null;
		this.btnReturn = null;
		this.hostCtn = null;
		this.joinCtn = null;
		this.timerCtn = null;
		this.intervalTimer = null;
		this.time = 0;

		this.create();
		this.addListeners();
		this.startTimer();
	}

	create(){
		this.mainpanel = document.createElement("div");
		this.mainpanel.className = "lobbyMainPanel";

		let title = document.createElement("div");
		title.textContent = "Partie #"+localStorage.getItem("id-partie");
		title.style.height = "50px";
		title.style.fontSize = "20px";
		this.mainpanel.appendChild(title);

		this.mainpanel.appendChild(this.createLabel("La partie commencera bientôt!"));
		this.mainpanel.appendChild(this.createLabel("En attente d'un autre joueur..."));

		this.panelPlayers = document.createElement("div");
		this.panelPlayers.className = "lobbyPlayersPanel";

		this.mainpanel.appendChild(this.panelPlayers);

		this.btnReturn = document.createElement("button");
		this.btnReturn.textContent = "Quitter";
		this.mainpanel.appendChild(this.btnReturn);

		this.timerCtn = document.createElement("div");
		this.timerCtn.className = "lobbyMainPanel_timer";
		this.mainpanel.appendChild(this.timerCtn);

		document.body.appendChild(this.mainpanel);
	}

	createLabel(message){
		let label = document.createElement("div");
		label.textContent = message;
		label.style.margin = "5px";
		return label;
	}

	addListeners(){
		this.btnReturn.onclick = (e) =>{
			quitGame();
		}
	}

	startTimer(){
		this.timerCtn.textContent = this.time;
		this.intervalTimer = setInterval(()=>{
			this.time++;
			this.timerCtn.textContent = this.time;
		}, 1000);
	}

	update(data){
		if(data.host && !this.hostCtn){
			this.hostCtn = this.createPlayer(data.host);
			this.panelPlayers.appendChild(this.hostCtn);
		}
		if(data.join && !this.joinCtn){
			this.joinCtn = this.createPlayer(data.join);
			this.panelPlayers.appendChild(this.joinCtn);
		}
	}

	createPlayer(player){
		let ctn = document.createElement("div");
		ctn.className = "lobbyPlayersPanel-player";
		let labelUsername = this.createLabel(player.username);
		ctn.appendChild(labelUsername);
		return ctn;
	}
	
	remove(){
		$(".lobbyMainPanel").fadeOut(1000, ()=>{
			this.mainpanel.remove();
		});
	}

	hide(){

	}
}