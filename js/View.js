/*
	Certaines parties du code ont été trouvés sur :
	https://www.learnthreejs.com/load-3d-model-using-three-js-obj-loader/
*/

class View{
	constructor(spriteList,playerList){
		this.controls = null;
		this.gameStats = null;
		this.line = null;
		this.init(spriteList);
	}
	init(spriteList){
		camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 20000 );

		scene = new THREE.Scene();
		//scene.fog = new THREE.Fog(0x000000, 0.015, 5000);
		scene.background = null;
		
		
		renderer = new THREE.WebGLRenderer( { alpha: true} );

		renderer.setPixelRatio(2.25); //MEILLEURE QUALITÉ
		renderer.setClearColor( 0x000000, 0 );
		renderer.setSize( window.innerWidth, window.innerHeight );
		document.body.appendChild( renderer.domElement );

		raycaster = new THREE.Raycaster();
		mouse = new THREE.Vector2();

		window.addEventListener( 'resize', this.onWindowResize, false );

		//controls
		this.controls = new THREE.OrbitControls(camera, renderer.domElement);
		this.controls.enableDamping = true;
		this.controls.dampingFactor = 0.25;
		this.controls.enableZoom = true;
		this.controls.autoRotate = true;

		//lumiere
		let keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 75%)'), 1.0);
		keyLight.position.set(-100, 0, 100);
		let fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 75%)'), 0.75);
		fillLight.position.set(100, 0, 100);
		var backLight = new THREE.DirectionalLight(0xffffff, 1.0);
		backLight.position.set(100, 0, -100).normalize();
		scene.add(keyLight);
		scene.add(fillLight);
		scene.add(backLight);

		this.import3dModel(spriteList);
		this.import3dWall();
		this.importPlayerStick();
		//importer model 3d
		
		this.positionCamDepart();
		// camera.position.z = 500;
		// camera.position.y = 200;

		//this.gameStats = new GameStats(playerList);
		
		this.createInitSurface();
		this.createSkyBox();
	}
	initGameStats(playerList){
		this.gameStats = new GameStats(playerList);
	}
	createInitSurface(){
		//table
		let texture = new THREE.TextureLoader().load("images/football_field.png");
		let geometry = new THREE.BoxBufferGeometry(5000,20,3000);
		let material = new THREE.MeshBasicMaterial({map:texture});
		let mesh = new THREE.Mesh(geometry, material);
		scene.add(mesh);
	}
	positionCamDepart(){
		camera.position.x = 1.93e-17;
		camera.position.y = 2000;
		camera.position.z = 2500;
		camera.rotation.x = 0;
		camera.rotation.y = 7.32e-17;
		camera.rotation.z = 3.90e-17;
	}

	stopCamRotation(){
		this.controls.autoRotate = false;
		camera.position.y *= 3;
		camera.position.z *= 3;
	}

	createSkyBox(){

	}
	import3dModel(spriteList){
		//soccerball
		let mtlLoader = new THREE.MTLLoader();
		mtlLoader.setTexturePath('/carom/images/3d/');
		mtlLoader.setPath('/carom/images/3d/');
		mtlLoader.load('football-soccer-ball.mtl', function (materials) {
			materials.preload();		
			let objLoader = new THREE.OBJLoader();
			objLoader.setMaterials(materials);
			objLoader.setPath('/carom/images/3d/');
			objLoader.load('football-soccer-ball.obj', function (object) {
				scene.add(object);
				object.position.y += 70;
				object.position.z += 100;
				for(let i = 0; i < spriteList.length; i++){
					const b = spriteList[i];
					if(b.color == "white"){
						b.obj = object;
						b.setPosition();
					}
				}
			});		
		});
		// redball
		mtlLoader.load('football-soccer-ball2.mtl', function (materials) {
			materials.preload();		
			let objLoader = new THREE.OBJLoader();
			objLoader.setMaterials(materials);
			objLoader.setPath('/carom/images/3d/');
			objLoader.load('football-soccer-ball2.obj', function (object) {
				scene.add(object);
				object.position.y += 70;
				object.position.x += 300;
				for(let i = 0; i < spriteList.length; i++){
					const b = spriteList[i];
					if(b.color == "red"){
						b.obj = object;
						b.setPosition();
					}
				}
			});		
		});

		mtlLoader.load('football-soccer-ball_yellow.mtl', function (materials) {
			materials.preload();		
			let objLoader = new THREE.OBJLoader();
			objLoader.setMaterials(materials);
			objLoader.setPath('/carom/images/3d/');
			objLoader.load('football-soccer-ball_yellow.obj', function (object) {
				scene.add(object);
				object.position.y += 70;
				object.position.x += 300;
				for(let i = 0; i < spriteList.length; i++){
					const b = spriteList[i];
					if(b.color == "yellow"){
						b.obj = object;
						b.setPosition();
					}
				}
			});
		});
		

	}
	import3dWall(){
		let mtlLoader = new THREE.MTLLoader();
		mtlLoader.setTexturePath('/carom/images/3d/');
		mtlLoader.setPath('/carom/images/3d/');
		mtlLoader.load('wooden-wall.mtl', function (materials) {
			materials.preload();		
			let objLoader = new THREE.OBJLoader();
			objLoader.setMaterials(materials);
			objLoader.setPath('/carom/images/3d/');
			objLoader.load('wooden-wall.obj', function (object) {
				//scene.add(object);
				let wallArray = getWallArray();
				for(let i = 0; i < wallArray.length;i++){
					const info = wallArray[i];
					let obj = object.clone();
					obj.position.x = info.posx;
					obj.position.y = info.posy;
					obj.position.z = info.posz;
					obj.rotation.y = info.roty;
					obj.scale.x = info.scalex;
					obj.scale.y = info.scaley;
					obj.scale.z = info.scalez;
					boardList.push(obj);
					scene.add(obj);
				}	

			});		
		});
	}
	importPlayerStick(){
		let mtlLoader = new THREE.MTLLoader();
		mtlLoader.setTexturePath('/carom/images/3d/player/');
		mtlLoader.setPath('/carom/images/3d/player/');
		mtlLoader.load('football-player-tt.mtl', function (materials) {
			materials.preload();		
			let objLoader = new THREE.OBJLoader();
			objLoader.setMaterials(materials);
			objLoader.setPath('/carom/images/3d/player/');
			objLoader.load('football-player-tt.obj', function (object) {
				scene.add(object);	
				object.scale.x = 300;
				object.scale.y = 300;
				object.scale.z = 300;				
				playerStick = new PlayerStick(object);

			});		
		});
		this.createGuideLine();
	}

	playStickAnimation(angle, force){
		// let bx = ball.obj.position.x ;
		// let by = ball.obj.position.z ;
		// let px = playerStick.obj.position.x;
		// let py = playerStick.obj.position.y;
		// let stepX = (px - bx)/100;
		// let stepY = (py - by)/100;
		// let newx = playerStick.obj.position.x;
		// let newy = playerStick.obj.position.y;
		// let interval = setInterval(()=>{
		// 	newx += stepX;
		// 	newy += stepY;
		// 	if(newx == bx && newy == by){
		// 		clearInterval(interval);
		// 		this.hidePlayerStick();
		// 	}
		// 	playerStick.obj.position.x = newx;
		// 	playerStick.obj.position.z = newy;
		// },100);

		let forceInit = force;
		let interval = setInterval(()=>{
			force -= 2;
			if(force <= 0){
				let data = {
					currentForce: forceInit,
					currentAngle: angle
				  };
				let event = new CustomEvent('hitBall', { detail: data });
				window.dispatchEvent(event);
				this.hidePlayerStick();
				clearInterval(interval);
			}
			this.movePlayerStick(angle, force);
		},10);
	}

	hidePlayerStick(){
		playerStick.obj.visible = false;
		guideLine.obj.visible = false;
	}

	showPlayerStick(){
		this.movePlayerStick(0,10);
		playerStick.obj.visible = true;
		guideLine.obj.visible = true;
	}

	createGuideLine(startPos=null, endPos=null){
		var positions = [];
		var colors = [];
		positions.push(0,300,0);
		positions.push(0,700,0);
		colors.push(0,1.0,1.0);
		colors.push(0,1.0,1.0);

		let geometry = new THREE.LineGeometry();
		geometry.setPositions( positions );
		geometry.setColors( colors );
		let matLine = new THREE.LineMaterial( {
			color: 0xffffff,
			linewidth: 3, // in pixels
			vertexColors: THREE.VertexColors,
			//resolution:  // to be set by renderer, eventually
			dashed: true,
		} );
		matLine.resolution.set( window.innerWidth, window.innerHeight );
		let line = new THREE.Line2( geometry, matLine );
		line.computeLineDistances();
		line.scale.set( 1, 1, 1 );
		scene.add( line );
		guideLine = new GuideLine(line);

		//ESSAI 3
		// var geometryCube = this.cube( 1000 );
		// var lineSegments = new THREE.LineSegments( geometryCube, new THREE.LineDashedMaterial( { color: 0xffaa00, dashSize: 3, gapSize: 1, linewidth: 10 } ) );
		// lineSegments.computeLineDistances();
		// scene.add( lineSegments );
		// guideLine = new GuideLine(lineSegments);
	}

	cube( size ) {
		var h = size * 0.5;
		var geometry = new THREE.BufferGeometry();
		var position = [];
		position.push(
			-h, -h, -h,
			-h, h, -h,
			-h, h, -h,
			h, h, -h,
			h, h, -h,
			h, -h, -h,
			h, -h, -h,
			-h, -h, -h,
			-h, -h, h,
			-h, h, h,
			-h, h, h,
			h, h, h,
			h, h, h,
			h, -h, h,
			h, -h, h,
			-h, -h, h,
			-h, -h, -h,
			-h, -h, h,
			-h, h, -h,
			-h, h, h,
			h, h, -h,
			h, h, h,
			h, -h, -h,
			h, -h, h
		 );
		geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( position, 3 ) );
		return geometry;
	}

	tick(){	
		this.controls.update();
		if(this.gameStats){
			this.gameStats.updatePanel();
		}
	
		renderer.render( scene, camera );
	}
	
	onWindowResize() {
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize( window.innerWidth, window.innerHeight );
	}

	blinkBoard(){
		let color = 0xff0000;
		for(let i = 0; i < boardList.length; i++){
			const b = boardList[i];
			for(let j = 0; j < b.children.length;j++){
				const m = b.children[j];
				m.material.color.setHex(color);					
			}
		}
		setTimeout(() => {
			this.resetBlink();
		}, 250);
	}
	resetBlink(){
		let color = 0xffffff;
		for(let i = 0; i < boardList.length; i++){
			const b = boardList[i];
			for(let j = 0; j < b.children.length;j++){
				const m = b.children[j];
				m.material.color.setHex(color);					
			}
		}
	}

	movePlayerStick(degree, pourcentageDistance){
		
		let radians = getRadians(degree);

		let ball = null;
		for(let i = 0; i < playerList.length; i++){
			const player = playerList[i];
			if(player.me && player.myturn){
				//le joueur peut bouger son baton!!!!
				let ballcolor = player.ballColor;
				for(let j = 0; j < spriteList.length; j++){
					const b = spriteList[j];
					if(ballcolor === b.color){
						ball = b;
					}
				}
			}
		}
		
		// for(let i = 0; i < spriteList.length; i++){
		// 	if(spriteList[i].color === "white"){
		// 		ball = spriteList[i];
		// 	}
		// }
		
		if(ball && ball.obj && playerStick){
			if(playerStick.obj){
				let len = (1000*pourcentageDistance/100)+100;

				let cx = ball.obj.position.x ;
				let cy = ball.obj.position.z ;

				let newx = (Math.cos(radians)*len)+cx;
				let newy = (Math.sin(radians)*len)+cy;
	
				playerStick.obj.position.x = newx;
				playerStick.obj.position.z = newy;
				let vec = new THREE.Vector3(ball.obj.position.x,0,ball.obj.position.z);
				playerStick.obj.lookAt(vec);

				//guideLine.obj.position.x = ball.obj.position.x;
				// guideLine.obj.position.z = ball.obj.position.z;
				//guideLine.obj.geometry.boundingBox.max.y += 10;
				// guideLine.obj.geometry.index.dynamic = true;
				var positions = [];
				radians = getRadians(degree + 180);
				let radX = Math.cos(radians);
				let radY = Math.sin(radians);
				let xStart = (radX*100)+cx;
				let yStart = (radY*100)+cy;
				let lineDirectionX = (radX*len*(pourcentageDistance/10))+cx;
				let lineDirectionY = (radY*len*(pourcentageDistance/10))+cy;
				positions.push(xStart,ball.obj.position.y,yStart);
				positions.push(lineDirectionX, ball.obj.position.y, lineDirectionY);
				guideLine.obj.geometry.setPositions(positions);
				// guideLine.obj.geometry.boundingBox.min = new THREE.Vector3(whiteball.obj.position.x,0,whiteball.obj.position.z);
			}
		}
	}
	
}

function getWallArray(){
	let wallArray = [];

	wallArray.push({posx : -2450, posy : 0, posz : -1450, roty : 0, scalex: 45, scaley:45, scalez:49 });
	wallArray.push({posx : -2450, posy : 0, posz :  -710, roty : 0, scalex: 45, scaley:45, scalez:49 });
	wallArray.push({posx : -2450, posy : 0, posz :    30, roty : 0, scalex: 45, scaley:45, scalez:49 });
	wallArray.push({posx : -2450, posy : 0, posz :   770, roty : 0, scalex: 45, scaley:45, scalez:49 });

	wallArray.push({posx : -1750, posy : 0, posz : -1475, roty : Math.PI/2*3, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx : -1060, posy : 0, posz : -1475, roty : Math.PI/2*3, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx :  -370, posy : 0, posz : -1475, roty : Math.PI/2*3, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx :   325, posy : 0, posz : -1475, roty : Math.PI/2*3, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx :  1020, posy : 0, posz : -1475, roty : Math.PI/2*3, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx :  1710, posy : 0, posz : -1475, roty : Math.PI/2*3, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx :  2400, posy : 0, posz : -1475, roty : Math.PI/2*3, scalex: 45, scaley:45, scalez:46 });

	wallArray.push({posx : 2450, posy : 0, posz : 1450, roty : Math.PI, scalex: 45, scaley:45, scalez:49 });
	wallArray.push({posx : 2450, posy : 0, posz :  710, roty : Math.PI, scalex: 45, scaley:45, scalez:49 });
	wallArray.push({posx : 2450, posy : 0, posz :  -30, roty : Math.PI, scalex: 45, scaley:45, scalez:49 });
	wallArray.push({posx : 2450, posy : 0, posz : -770, roty : Math.PI, scalex: 45, scaley:45, scalez:49 });

	wallArray.push({posx : 1750, posy : 0, posz : 1475, roty : Math.PI/2, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx : 1060, posy : 0, posz : 1475, roty : Math.PI/2, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx :  370, posy : 0, posz : 1475, roty : Math.PI/2, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx : -325, posy : 0, posz : 1475, roty : Math.PI/2, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx :-1020, posy : 0, posz : 1475, roty : Math.PI/2, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx :-1710, posy : 0, posz : 1475, roty : Math.PI/2, scalex: 45, scaley:45, scalez:46 });
	wallArray.push({posx :-2400, posy : 0, posz : 1475, roty : Math.PI/2, scalex: 45, scaley:45, scalez:46 });
	

	return wallArray;
}
