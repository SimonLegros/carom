class GameStats{
	constructor(){
		this.player1div = null;
		this.player1score = null
		this.player2div = null;
		this.player2score = null
		this.timestatsdiv = null;
		this.btnQuit = null;
		this.currentTime = 0; //en secondes

		this.divPrincipal = document.createElement("div");
		this.divPrincipal.setAttribute("id","gamestats");
		document.body.appendChild(this.divPrincipal);

		this.builtPanel();
		this.addListeners();
		this.debutPartie = new Date().getTime();
		this.intervalTimer = setInterval(()=>{
			this.updateTimer();
		},1000);
	}

	updateTimer(){
		this.currentTime++;
		this.timestatsdiv.textContent = this.toTime(this.currentTime);
	}
	toTime(sec){
		let h = Math.floor(sec / 3600);
		let m = Math.floor(sec / 60);
		let s = sec % 60;
		h = (h <= 9)? "0"+h: h;
		m = (m <= 9) ? "0" + m : m;
		s = (s <= 9) ? "0" + s : s;
		return h + ":" + m + ":" + s;
	}

	builtPanel(){

		//insertion des stats du player1
		this.player1div = document.createElement("div");
		this.player1div.className = "playerstats";
		this.player1div.setAttribute("id","player1div");
		this.player1div.innerHTML = playerList[0].name;
		this.player1score = document.createElement("div");
		this.player1score.innerHTML = "score : " + playerList[0].score;
		
		this.player1div.appendChild(this.player1score);
		this.divPrincipal.appendChild(this.player1div);

		//insertion des stats du player2
		this.player2div = document.createElement("div");
		this.player2div.className = "playerstats";
		this.player2div.setAttribute("id","player2div");
		this.player2div.innerHTML = playerList[1].name;
		this.player2score = document.createElement("div");
		this.player2score.innerHTML = "score : " + playerList[1].score;
		this.player2div.appendChild(this.player2score);
		this.divPrincipal.appendChild(this.player2div);

		//insertion des stats pour le temps
		this.timestatsdiv = document.createElement("div");
		this.timestatsdiv.className = "timerstats";
		this.timestatsdiv.innerHTML = "00:00:00"
		this.divPrincipal.appendChild(this.timestatsdiv);

		this.btnQuit = document.createElement("button");
		this.btnQuit.textContent = "Quit";
		this.divPrincipal.appendChild(this.btnQuit);

	}
	updatePanel(){
		//set des couleur de bordure
		let color1 = "red";
		let color2 = "green";
		if(playerList[0].myturn){
			color1 = "green";
			color2 = "red";
		}
		document.getElementById("player1div").style.borderColor = color1;
		document.getElementById("player2div").style.borderColor = color2;
		let now = new Date().getTime();
		this.player1score.innerHTML = "score : " + playerList[0].score;
		this.player2score.innerHTML = "score : " + playerList[1].score;
	}

	addListeners(){
		this.btnQuit.onclick = (e) =>{
			quitGame();
		}
	}
}