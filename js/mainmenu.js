
const 	PARTIE_LIBRE = 0,
		PARTIE_1_BANDE = 1,
		PARTIE_3_BANDES = 2;

window.onload = function(){

	$(".mainmenu").fadeIn(1000);

	let text = "Welcome, "+getCookie("username")+"!";
	document.querySelector("#mainmenu_welcom").textContent = text;

	$("#btnLibre").click(()=>{
		findMatch(PARTIE_LIBRE);
	});
	$("#btn_1Bande").click(()=>{
		findMatch(PARTIE_1_BANDE);
	});
	$("#btn_3Bandes").click(()=>{
		findMatch(PARTIE_3_BANDES);
	});

}
let type_partie = null;
function findMatch(type) {
	let formData = new FormData();
	formData.append("type-partie", type);
	localStorage.setItem('type-partie', type);

	fetch('ajaxChooseGame.php',{
		method : "POST",
		credentials: "include",
		body: formData
	})
	.then(function(reponse){
		reponse.text().then(function(data){
			data = JSON.parse(data);
			localStorage.setItem('id-partie', data);
			window.location.href = "game.php";
		})
	})

}

