class Ball {
	constructor(colorId){
		let colorList = ["white","yellow","red"];
		this.obj = null;
		this.color = colorList[colorId];
		this.velocityZ = 0;
		this.velocityX = 0;
		this.mass = 2;

		this.rotX = 20;
		this.rotZ = 20;

		this.hitBy = null;
		this.hitTick = 0;
		this.WAITHIT = 10;

		this.COEFFICIENTfRICTION = 1.005;

		this.isInMotion = true;
	}

	tick(){

		if (this.obj) {
			this.obj.rotation.x += this.velocityX /  this.rotX;
			this.obj.rotation.z +=  this.velocityZ / this.rotZ;
			
			this.obj.position.x += this.velocityX
			this.obj.position.z += this.velocityZ
			
			this.friction()
		}
		

		if(this.hitBy != null){
			if (this.hitTick >= this.WAITHIT) {
				this.resetHitTick();
			}else{
				this.hitTick++;
			}
		}

		this.isInMotion = this.checkMotion();


	}

	checkMotion(){
		let motionState = true;
		if (this.velocityX == 0) {
			if(this.velocityZ == 0){
				motionState = false
			}
		}
		return motionState;
	}

	resetHitTick(){
		this.hitBy = null;
		this.hitTick = 0;
	}

	registerHit(ball){
		ball.resetHitTick();
		ball.hitBy = this.color;

	}

	isAlreadyHit(ball){
		let isAlreadyHit = false;

		if (ball.color == this.hitBy) {
			isAlreadyHit = true;
		}

		return isAlreadyHit;
	}

	friction(){
		this.velocityX = this.bringToZero(this.velocityX)
		this.velocityZ = this.bringToZero(this.velocityZ)
	}

	bringToZero(value){
		if(value != 0){
			value /= this.COEFFICIENTfRICTION;
		} 
		if(Math.abs(value) < 0.1 ){
			value = 0;
		}
		return value;
	}
	setPosition(){
		if(this.color === "white"){
			this.obj.position.x = 1700;
			this.obj.position.z = -350;
		}
		else if(this.color === "red"){
			this.obj.position.x = -1700;
			this.obj.position.z = 0;
		}
		else if(this.color === "yellow"){
			this.obj.position.x = 1700;
			this.obj.position.z = 350;
		}
	}
}