let view = null;
let camera = null;
let scene = null;
let renderer = null;
let mouse = null;
let raycaster = null;
let spriteList = [];
let playerStick = null;
let playerList = [];
let boardList = [];
let guideLine = null;
let gameStats = null;
let gameControls = null;
let lobbyPanel = null;
let test = true;
let gameIsOn = false;
let idActionCourant = null;

let tableInMotion = true;

let gestionPartie = null;

let firstTime = true;
let dataSended = false;

window.onload = function(){


	spriteList.push(new Ball(0));
	spriteList.push(new Ball(2));
	spriteList.push(new Ball(1));

	lobbyPanel = new LobbyPanel();
	gameControls = new GameControls();

	view = new View(spriteList,playerList);
	// gestionPartie = new GestionPartie(playerList[0],playerList[1],1);


	$(window).on('unload', function () {
		quitGame();
		for(let i = 0; i < 1000; i++){
			console.log("");
		}
	});

	$(window).on('beforeunload', function () {
		return 'Are you sure you want to leave?';
	});
	
	//Appele au 2 sec une requete d'info du jeu
	setTimeout(getInfos, 2000);
	// Il y a une attente de 20 sec en recherche d'un autre joueur
	// Sinon on quitte le jeu
	setTimeout(()=>{
		if(!gameIsOn){
			quitGame();
		}
	}, 1000 * 30)
	//Tick de l'interface
	mainTick();
}

function mainTick(){
	view.tick();
	//view.movePlayerStick(1,100);
	if(gameIsOn){
		
		if (firstTime) {
			firstTime = false;
			gestionPartie = new GestionPartie(playerList[0],playerList[1],localStorage.getItem('type-partie'));
		}
		for(let i = 0; i < spriteList.length; i++){
			const b = spriteList[i];
			b.tick();
			collisionMur(b)
			if (b.obj) {
				checkCollisionEntreBallons(b)
			}
		}
		let newState = checkTableInMotion()
		if(tableInMotion != newState){
			tableInMotion = newState;
			if(!tableInMotion){
				dispatchEvent_TurnComplete();
			}
		}
		// tableInMotion = checkTableInMotion()
		// if(!dataSended && !tableInMotion){
			// let event = new CustomEvent('turnComplete');
			// window.dispatchEvent(event);
		// }

	}
		
	requestID = requestAnimationFrame(mainTick);
}

function dispatchEvent_TurnComplete(){
	let event = new CustomEvent('turnComplete');
	window.dispatchEvent(event);
	dataSended = true;
}

window.addEventListener('hitBall', function (e) {
	if (!tableInMotion) {
		let player = getHitPlayer();
		if(player.me){
			let vector = convertIntoVectors(e.detail.currentAngle,e.detail.currentForce)
			let ball = getBall(player.ballColor);
			let x = ball.obj.position.x;
			let y = ball.obj.position.y;
			
			addAction(e.detail.currentAngle, e.detail.currentForce, x, y);
			hitBall(vector[0],vector[1], ball.color);
		}
	}else{
		// console.log("Table still in motion")
	}
});

function getHitPlayer(){
	for (let i = 0; i < playerList.length; i++) {
		const player = playerList[i];
		if(player.myturn){
			return player;
		}
	}
}

function getBall(color){
	let ballreturn = null;
	spriteList.forEach(ball => {
		if (ball.color === color) {
			ballreturn = ball;
		}
	});
	return ballreturn;
}

function convertIntoVectors(angle,distance){
	angle += 180;
	angle = angle * Math.PI / 180;
	// console.log(angle, distance);
	x = distance*Math.cos(angle);
	y = distance*Math.sin(angle);
	return [x,y];
}

function hitBall(x,z, color){
	spriteList.forEach(ball => {
		if (ball.color == color) {
			ball.velocityX = x;
			ball.velocityZ = z;
		}
	});
}

function checkTableInMotion(){
	motion = false;
	spriteList.forEach(ball => {
		if(ball.isInMotion){
			motion = true;
		}
	});

	return motion;
}

function createPlayer(data){
	let nomCookie = getCookie("username");
	let player1 = null;
	let nom1 = data.host.username;
	let nom2 = data.join.username;
	// console.log(nom1 + "/" + nom2);
	let player2 = null;
	
	//host est toujours le player1
	if(nomCookie == nom1){
		//veut dire que le host est le player1
		player1 = new Player(nom1, true, true);
		player2 = new Player(nom2, false, false);
	}
	else if(nomCookie == nom2){
		//veut dire que le host est le player2
		player1 = new Player(nom1, false, true);
		player2 = new Player(nom2, true, false);
	}
	playerList.push(player1);
	playerList.push(player2);
	view.initGameStats(playerList);
	
}

function removePlayer(name){
	for (let i = 0; i < playerList.length; i++) {
		if(playerList[i].name == name){
			playerList.splice(i, 1);
		}
	}
}

function quitGame(){
	let formData = new FormData();
	formData.append("id-partie", localStorage.getItem("id-partie"));
	fetch('ajaxQuitGame.php',{
		method : "POST",
		credentials: "include",
		body: formData
	})
	.then(function(reponse){
		reponse.text().then(function(data){
			// console.log(data);
			data = JSON.parse(data);
			localStorage.removeItem("id-partie");
			window.location.href = "mainmenu.php";
		});
	});
}

function addAction(angle, force, x, y){
	let formData = new FormData();
	formData.append("angle", angle);
	formData.append("force", force);
	formData.append("x", x);
	formData.append("y", y);
	formData.append("idPartie", localStorage.getItem("id-partie"))

	fetch('ajaxAddAction.php', {
		method: "POST",
		credentials: "include",
		body: formData
	})
	.then(function(reponse){
		reponse.text().then(function(data){
			// console.log(data);
		})
	})
}

function getInfos(){
	// console.log("get Info");
	let formData = new FormData();
	formData.append("idPartie", localStorage.getItem('id-partie'));
	
	fetch('ajaxInfosGame.php', {
		method: "POST",
		credentials: "include",
		body: formData
	})
	.then(function(reponse){
		reponse.text().then(function(data){
			data = JSON.parse(data);
			//État de la partie.
			if(data.state == 1){
				if(!gameIsOn){
					gameIsOn = true;
					createPlayer(data);

					lobbyPanel.remove();
					lobbyPanel = null;
					view.stopCamRotation();
				}
				else{
					if(data.action.id != idActionCourant){
						idActionCourant = data.action.id;
						for (let i = 0; i < playerList.length; i++) {
							const player = playerList[i];
							if(player.me && !player.myturn){
								if(player.ballColor == 'white'){
									let ball = getBall('yellow');
									let vector = convertIntoVectors(data.action.angle, data.action.force);
									hitBall(vector[0],vector[1], ball.color);
								}
								else{
									let ball = getBall('white');
									let vector = convertIntoVectors(data.action.angle, data.action.force);
									hitBall(vector[0],vector[1], ball.color);
								}
								// Rotation du joueur.

							}
						}
					}
				}
			}
			else if(data.state == 2){
				quitGame();
			}
			else{
				lobbyPanel.update(data);
			}
			setTimeout(getInfos, 2000);
		})
	});
}

function collisionMur(ball){
	obj = ball.obj
	let p = getHitPlayer();
	let boardHit = false;
	if(obj){
		if(obj.position.z > 1400){
			obj.position.z = 1399;
			ball.velocityZ = -ball.velocityZ;
			ball.rotX = - ball.rotX;
			if(p.ballColor === ball.color){
				boardHit = true;
			}
		}
		if (obj.position.z < -1400){
			obj.position.z = -1399;
			ball.velocityZ = Math.abs(ball.velocityZ);
			ball.rotX = Math.abs(ball.rotX);
			if(p.ballColor === ball.color){
				boardHit = true;
			}
		}
		if(obj.position.x > 2400){
			obj.position.x = 2399;
			ball.velocityX = - ball.velocityX;
			ball.rotZ = Math.abs(ball.rotZ);
			if(p.ballColor === ball.color){
				boardHit = true;
			}
		}
		if(obj.position.x < -2400){
			obj.position.x = -2399;
			ball.velocityX = Math.abs(ball.velocityX);
			ball.rotZ = -ball.rotZ;
			if(p.ballColor === ball.color){
				boardHit = true;
			}
		}
		if(boardHit){
			console.log("------------------------------------------")
			console.log(ball)
			let data = {
				ball: ball
			  };
			let event = new CustomEvent('collisionMur',{ detail: data });
			window.dispatchEvent(event);
			view.blinkBoard();
		}
	}
}

function checkCollisionEntreBallons(ball){
	spriteList.forEach(element => {
		if(element.obj){
			if(ball.color != element.color){
				if(checkCollision(ball,element)){
					if (!(ball.isAlreadyHit(element))) {
						registerHit(ball,element);
						manage_bounce(ball,element);
					}
				}
			}
		}
	});
}

function registerHit(ballA,ballB){
	let data = {
		ballA: ballA,
		ballB: ballB
	  };
	let event = new CustomEvent('collisionBall', { detail: data });
	window.dispatchEvent(event);
	ballA.registerHit(ballB);
	ballB.registerHit(ballA);
}

function checkCollision(ballA, ballB){
	let collision = false;
	if(ballA.obj.children[0].geometry.boundingSphere && ballB.obj.children[0].geometry.boundingSphere){
		let fixRadius = 0;
		let radiusA = ballA.obj.children[0].geometry.boundingSphere.radius-fixRadius;

		let radiusB = ballB.obj.children[0].geometry.boundingSphere.radius-fixRadius;

		let infoA = {radius : radiusA, x : ballA.obj.position.x, z: ballA.obj.position.z};
		let infoB = {radius : radiusB, x : ballB.obj.position.x, z: ballB.obj.position.z};

		let diffx = infoA.x - infoB.x;
		let diffz = infoA.z - infoB.z;
		let distance = Math.sqrt(diffx * diffx + diffz * diffz);

		if(distance < infoA.radius + infoB.radius){
			collision = true;
		}
	}

	return collision;
}

// Elastic collision
// http://jsfiddle.net/inkfood/juzsR/
function manage_bounce(ballA,ballB){
	let dx = ballA.obj.position.x-ballB.obj.position.x;
	let dy = ballA.obj.position.y-ballB.obj.position.y;
	//dx = ball.left-ball2.left;
    //dy = ball.top-ball2.top;

	let collision_angle = Math.atan2(dy, dx);
	//collisionision_angle = Math.atan2(dy, dx);

    let magnitude_1 = Math.sqrt(ballA.velocityX*ballA.velocityX+ballA.velocityZ*ballA.velocityZ);
	let magnitude_2 = Math.sqrt(ballB.velocityX*ballB.velocityX+ballB.velocityZ*ballB.velocityZ);
	//magnitude_1 = Math.sqrt(ball.xspeed*ball.xspeed+ball.yspeed*ball.yspeed);
    //magnitude_2 = Math.sqrt(ball2.xspeed*ball2.xspeed+ball2.yspeed*ball2.yspeed);

    let direction_1 = Math.atan2(ballA.velocityZ, ballA.velocityX);
	let direction_2 = Math.atan2(ballB.velocityZ, ballB.velocityX);
	//direction_1 = Math.atan2(ball.yspeed, ball.xspeed);
    //direction_2 = Math.atan2(ball2.yspeed, ball2.xspeed);

    let new_xspeed_1 = magnitude_1*Math.cos(direction_1-collision_angle);
	let new_yspeed_1 = magnitude_1*Math.sin(direction_1-collision_angle);
	//new_xspeed_1 = magnitude_1*Math.cos(direction_1-collisionision_angle);
    //new_yspeed_1 = magnitude_1*Math.sin(direction_1-collisionision_angle);

    let new_xspeed_2 = magnitude_2*Math.cos(direction_2-collision_angle);
	let new_yspeed_2 = magnitude_2*Math.sin(direction_2-collision_angle);
	//new_xspeed_2 = magnitude_2*Math.cos(direction_2-collisionision_angle);
    //new_yspeed_2 = magnitude_2*Math.sin(direction_2-collisionision_angle);

    let final_xspeed_1 = ((ballA.mass-ballB.mass)*new_xspeed_1+(ballB.mass+ballB.mass)*new_xspeed_2)/(ballA.mass+ballB.mass);
	let final_xspeed_2 = ((ballA.mass+ballA.mass)*new_xspeed_1+(ballB.mass-ballA.mass)*new_xspeed_2)/(ballA.mass+ballB.mass);
	//final_xspeed_1 = ((ball.mass-ball2.mass)*new_xspeed_1+(ball2.mass+ball2.mass)*new_xspeed_2)/(ball.mass+ball2.mass);
	//final_xspeed_2 = ((ball.mass+ball.mass)*new_xspeed_1+(ball2.mass-ball.mass)*new_xspeed_2)/(ball.mass+ball2.mass);
	
    let final_yspeed_1 = new_yspeed_1;
	let final_yspeed_2 = new_yspeed_2;
	//final_yspeed_1 = new_yspeed_1;
	//final_yspeed_2 = new_yspeed_2;
	
    ballA.velocityX = Math.cos(collision_angle)*final_xspeed_1+Math.cos(collision_angle+Math.PI/2)*final_yspeed_1;
	ballA.velocityZ = Math.sin(collision_angle)*final_xspeed_1+Math.sin(collision_angle+Math.PI/2)*final_yspeed_1;
	//ball.xspeed = Math.cos(collisionision_angle)*final_xspeed_1+Math.cos(collisionision_angle+Math.PI/2)*final_yspeed_1;
	//ball.yspeed = Math.sin(collisionision_angle)*final_xspeed_1+Math.sin(collisionision_angle+Math.PI/2)*final_yspeed_1;
	
    ballB.velocityX = Math.cos(collision_angle)*final_xspeed_2+Math.cos(collision_angle+Math.PI/2)*final_yspeed_2;
	ballB.velocityZ = Math.sin(collision_angle)*final_xspeed_2+Math.sin(collision_angle+Math.PI/2)*final_yspeed_2;
	//ball2.xspeed = Math.cos(collisionision_angle)*final_xspeed_2+Math.cos(collisionision_angle+Math.PI/2)*final_yspeed_2;
    //ball2.yspeed = Math.sin(collisionision_angle)*final_xspeed_2+Math.sin(collisionision_angle+Math.PI/2)*final_yspeed_2;
}
