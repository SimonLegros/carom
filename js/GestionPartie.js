class GestionPartie{
	constructor(PlayerOne,PlayerTwo,gameType){
		this.PlayerOne = PlayerOne;
		this.PlayerTwo = PlayerTwo;
		this.changingTurn = false;

		gameType = parseInt(gameType);
		// gameType -= 1;
		console.log(gameType)
		if(gameType == 0){
			this.sequence = [0,0];
		}else if (gameType == 1){
			this.sequence = [0,1,0];
		}else if (gameType == 2){
			this.sequence = [0,1,1,1,0];
		}
		
		// this.sequence = [0,0];

		this.currentSequence = [];
		this.sequenceHappen = false;

		if(PlayerOne.myturn){
			this.playerTurn = this.PlayerOne;
		}else{
			this.playerTurn = this.PlayerTwo;
		}

		this.lastBall = null;

		this.hitTick = 0;
		this.WAITHIT = 10;

	}

	reset() {
		this.currentSequence = [];
		this.sequenceHappen = false;
		this.lastBall = null;
	}

	changeTurn(){
		if(this.PlayerOne && this.PlayerTwo){
			if(this.PlayerOne.myturn){
				this.PlayerOne.myturn = false;
				this.PlayerTwo.myturn = true;
				this.playerTurn = this.PlayerTwo;
			}
			else{
				this.PlayerOne.myturn = true;
				this.PlayerTwo.myturn = false;
				this.playerTurn = this.PlayerOne;
			}
			if(this.playerTurn.me){
				gameControls.show();
				view.showPlayerStick();
			}
			else{
				gameControls.hide();
				view.hidePlayerStick();
			}
		}
	}

	registerImpactBall(ballA,ballB){
		if (!this.sequenceHappen) {
			let ballInPlay = null;

			if(ballA.color == this.playerTurn.ballColor){
				ballInPlay = ballB;
			}else if (ballB.color == this.playerTurn.ballColor){
				ballInPlay = ballA;
			}

			if(ballInPlay != null){
				if(ballInPlay != this.lastBall){
					this.lastBall = ballInPlay;
					// this.changeTurn();
					this.registerEvent(0);
				}else{
					this.reset();
				}
			}
		}
	}

	registerEvent(eventId){
		
		console.log(eventId)
		// if(eventId == 0){
		// 	if (this.currentSequence.length == 0) {
		// 		this.currentSequence.push(0);
		// 	}else {
		// 		this.currentSequence.push(0);
		// 		console.log("AKHFDKSHFDSHFJDFHDSHFJDSFGH")
		// 		console.log(this.currentSequence)
		// 		if (this.sequence.length == this.currentSequence.length) {
		// 			this.sequenceHappen = true;
		// 		}
		// 	}
		// }else {
		// 	if(this.currentSequence.length > 0){
		// 		if (this.hitTick >= this.WAITHIT) {
		// 			this.resetHitTick();
		// 		}else{
		// 			this.hitTick++;
		// 		}
		// 		if(this.hitTick == 0){
		// 			this.currentSequence.push(1);
		// 		}
				
		// 	}
		// }

		if(eventId == 0){
			this.currentSequence.push(0);
			let found = this.currentSequence.some(r=> this.sequence.includes(r));
			this.sequenceHappen = found;
		}
		
		if(this.sequenceHappen){
			this.reset();
			this.playerTurn.score += 1;
		}


		console.log("registerEvent")
		console.log(this.sequence)
	}

	__checkBallToWall(eventId,ball){
		if(this.playerTurn.ballColor == ball.color){
			this.registerEvent(1);
		}
	}

	resetHitTick(){
		this.hitTick = 0;
	}

	
}

window.addEventListener('collisionMur', function (e) {
	console.log(e.detail.ball);
	gestionPartie.__checkBallToWall(1,e.detail.ball);
	
});

window.addEventListener('collisionBall', function (e) {
	gestionPartie.registerImpactBall(e.detail.ballA,e.detail.ballB);
});

window.addEventListener('turnComplete', function (e) {
	gestionPartie.reset()
	gestionPartie.changeTurn();
});