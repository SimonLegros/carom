<?php
	require_once("action/GameAction.php");

	$action = new GameAction();
	$action->execute();

	require_once("partial/header.php");
?>
<script src="js/GameControls.js"></script>
<script src="js/three.js"></script>
<script src="js/hilbert3D.js"></script>
<script src='js/LineSegmentsGeometry.js'></script>
<script src='js/LineGeometry.js'></script>
<script src='js/WireframeGeometry2.js'></script>
<script src='js/LineMaterial.js'></script>
<script src='js/LineSegments2.js'></script>
<script src='js/Line2.js'></script>
<script src='js/Wireframe.js'></script>
<script src="js/caromGame.js"></script>
<script src="js/PlayerStick.js"></script>
<script src="js/GuideLine.js"></script>
<script src="js/GameStats.js"></script>
<script src="js/utils.js"></script>
<script src="js/Player.js"></script>
<script src="js/LobbyPanel.js"></script>
<script src="js/Ball.js"></script>
<script src="js/View.js"></script>
<script src="js/OBJLoader.js"></script>
<script src="js/MTLLoader.js"></script>
<script src="js/THREE.MeshLine.js"></script>
<script src="js/OrbitControls.js"></script>
<script src="js/GestionPartie.js"></script>
<script>

</script>
<?php
	require_once("partial/footer.php");