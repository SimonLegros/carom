<?php
	require_once("action/ChooseGameAction.php");

	$action = new ChooseGameAction();
	$action->execute();

	echo json_encode($action->result);