<?php
	require_once("action/registerAction.php");

	$action = new RegisterAction();
	$action->execute();

	echo json_encode($action->result);