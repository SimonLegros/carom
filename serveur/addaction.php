<?php
	require_once("action/AddActionAction.php");

	$action = new AddActionAction();
	$action->execute();

	echo json_encode($action->result);