<?php
	require_once("CommonAction.php");
	require_once("DAO/UserDAO.php");

	class LogoutAction extends CommonAction{

		public $result;

		public function __construct(){
			parent::__construct();
		}

		public function executeAction(){
			if(isset($_POST["idUser"]){
				UserDAO::logout($_POST["idUser"]);
			}
		}
	}