<?php
	require_once("CommonAction.php");
	require_once("DAO/GameDAO.php");

	class AddActionAction extends CommonAction{

		public $result;

		public function __construct(){
			parent::__construct();
		}

		public function executeAction(){
			if(isset($_POST["idUser"]) && isset($_POST["idPartie"]) && isset($_POST["angle"]) && 
			   isset($_POST["x"]) && isset($_POST["y"]) && isset($_POST["force"])){
				$this->result = GameDAO::addAction($_POST["idUser"], $_POST["idPartie"], $_POST["angle"], $_POST["x"], $_POST["y"], $_POST["force"]);
			}
		}
	}