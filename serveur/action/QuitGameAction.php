<?php
	require_once("CommonAction.php");
	require_once("DAO/UserDAO.php");

	class QuitGameAction extends CommonAction{

		public $result;

		public function __construct(){
			parent::__construct();
		}

		public function executeAction(){
			if(isset($_POST["idUser"]) && isset($_POST["idPartie"])){
				UserDAO::exitGame($_POST["idPartie"]);
			}
		}
	}
