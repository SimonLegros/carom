<?php
	require_once("CommonAction.php");
	require_once("DAO/UserDAO.php");

	class ChooseGameAction extends CommonAction{

		public $result;

		public function __construct(){
			parent::__construct();
		}

		public function executeAction(){
			if(isset($_POST["id"]) && isset($_POST["type-partie"])){
				$this->result = UserDAO::chooseGame($_POST["id"], $_POST["type-partie"]);
			}
			
		}
	}