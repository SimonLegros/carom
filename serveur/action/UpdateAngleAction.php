<?php
	require_once("CommonAction.php");
	require_once("DAO/GameDAO.php");

	class UpdateAngleAction extends CommonAction{
		
		public $result;

		public function __construct(){
			parent::__construct();
		}

		public function executeAction(){
			if(isset($_POST["angle"]) && isset($_POST["distance"]) && isset($_POST["idPartie"])){
				$this->result = GameDAO::updateAngle(intval($_POST["angle"]), intval($_POST["distance"]), intval($_POST["idPartie"]));
			}
		}
	}