<?php
	session_start();

	abstract class CommonAction {

		public function __construct() {

		}

		public function execute() {
			$this->executeAction();
		}

		protected abstract function executeAction();
	}