<?php
	require_once("CommonAction.php");
	require_once("DAO/UserDAO.php");

	class SigninAction extends CommonAction{
		
		public $result;

		public function __construct(){
			parent::__construct();
		}

		public function executeAction(){
			if(!empty($_POST["username"]) && !empty($_POST["password"])){
				$this->result = UserDAO::signin($_POST["username"], $_POST["password"]);
			}
		}
	}