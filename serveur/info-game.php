<?php
	require_once("action/InfoGameAction.php");
	
	$action = new InfoGameAction();
	$action->execute();

	echo json_encode($action->result);