<?php
	require_once("MyDB.php");
	
	class GameDAO{
		public static function getInfos($idPartie){
			//Infos de l'adversaire
			$db = new MyDB();
			$db->busyTimeout(500);
			$db->exec("BEGIN;");
			$statement = $db->prepare("SELECT username, niveau
									   FROM Users WHERE id IN
									   		(SELECT idJoueurHost
											 FROM Session WHERE id = ?)");
			$statement->bindValue(1, intval($idPartie));
			$result = $statement->execute();
			$host = $result->fetchArray();

			$statement = $db->prepare("SELECT username, niveau
									   FROM Users WHERE id IN
									   		(SELECT idJoueurJoin
											 FROM Session WHERE id = ?)");
			$statement->bindValue(1, intval($idPartie));
			$result = $statement->execute();
			$join = $result->fetchArray();

			$statement = $db->prepare("SELECT IdType, state, angleBaton, distance
										FROM Session WHERE id = ?");
			$statement->bindValue(1, intval($idPartie));
			$result = $statement->execute();
			$state = $result->fetchArray();

			$statement = $db->prepare("SELECT id, idJoueur, idParam
										FROM Action WHERE idSession = ?
										ORDER BY id DESC LIMIT 1");
			$statement->bindValue(1, intval($idPartie));
			$result = $statement->execute();
			$action = $result->fetchArray();
			
			$statement = $db->prepare("SELECT angle, force, x, y
									   FROM Parametres WHERE id = ?");
			$statement->bindValue(1, intval($action["IdParam"]));
			$result = $statement->execute();
			$param = $result->fetchArray();
			
			$infos = array(
				"type" => $state["IdType"],
				"host" => array(
					"username" => $host["username"],
					"level" => $host["niveau"]
				),
				"join" => array(
					"username" => $join["username"],
					"level" => $join["niveau"]
				),
				"state" => $state["state"],
				"angleBaton" => $state["angleBaton"],
				"distance" => $state["distance"],
				"action" => array(
					"id" => $action["Id"],
					"acteur" => $action["IdJoueur"],
					"angle" => $param["angle"],
					"force" => $param["force"],
					"x" => $param["x"],
					"y" => $param["y"]
				)
			);

			$db->exec("COMMIT;");
			$db->busyTimeout(500);
			$db->close();
			unset($db);

			return $infos;
		}

		public static function addAction($idUser, $idPartie, $angle , $x, $y, $force){
			$db = new MyDB();
			$db->busyTimeout(500);
			$db->exec("BEGIN;");
			$statement = $db->prepare("INSERT INTO Parametres(force, angle, x, y) VALUES(?, ?, ?, ?)");
			$statement->bindValue(1, intval($force));
			$statement->bindValue(2, intval($angle));
			$statement->bindValue(3, floatval($x));
			$statement->bindValue(4, floatval($y));
			$statement->execute();

			$statement = $db->prepare("SELECT id FROM Parametres ORDER BY id DESC LIMIT 1");
			$result = $statement->execute();

			$idParams = $result->fetchArray()["0"];

			$statement = $db->prepare("INSERT INTO Action(IdSession, IdJoueur, IdParam) VALUES(?, ?, ?)");
			$statement->bindValue(1, intval($idPartie));
			$statement->bindValue(2, intval($idUser));
			$statement->bindValue(3, intval($idParams));
			$statement->execute();

			$db->exec("COMMIT;");
			$db->busyTimeout(500);
			$db->close();
			unset($db);

			return "OK";
		}

		public static function updateAngle($angle, $distance, $idPartie){
			$db = new MyDB();
			$db->busyTimeout(500);
			$db->exec("BEGIN;");
			$statement = $db->prepare("UPDATE Session SET angleBaton = ?, distance = ? WHERE id = ?");
			$statement->bindValue(1, intval($angle));
			$statement->bindValue(2, intval($distance));
			$statement->bindValue(3, intval($idPartie));
			$statement->execute();

			$db->exec("COMMIT;");
			$db->busyTimeout(500);
			$db->close();
			unset($db);
			
			return $distance;
		}
	}
	