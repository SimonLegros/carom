<?php

	require_once("MyDB.php");

	class UserDAO {
		public static function signin($username, $password){
			$result = '';
			$db = new MyDB();
			$db->busyTimeout(500);
			$db->exec("BEGIN;");
			$statement = $db->prepare("SELECT id FROM Users WHERE UPPER(username) LIKE UPPER(?)");
			$statement->bindValue(1, $username);
			$result = $statement->execute();
			$id = $result->fetchArray()[0]; // Information obtenue.
			
			if(empty($id)){
				$id = 'USER_NOT_FOUND';
			}
			if(!(is_numeric($id))){
				$id = 'USER_NOT_FOUND';
			}
			else{
				$statement2 = $db->prepare("SELECT password FROM Users WHERE id = ?");
				$statement2->bindValue(1, $id);
				$result = $statement2->execute();
				$passwd = $result->fetchArray()[0]; // Information obtenue.
				if(!($passwd === $password)){
					$id = 'PASSWORD_INVALID';
				}
			}

			$db->exec("COMMIT;");
			$db->busyTimeout(500);
			$db->close();
			unset($db);
			
			return $id;
		}

		public static function addUser($username, $password){
			$result = '';
			$success = '';
			$db = new MyDB();
			$db->busyTimeout(500);
			$db->exec("BEGIN;");
			$statement = $db->prepare("SELECT COUNT(Id) FROM Users WHERE UPPER(username) LIKE UPPER(?)");
			$statement->bindValue(1, $username);
			$result = $statement->execute();
			$nb = $result->fetchArray()[0]; // Information obtenue.

			if($nb === 0){
				$statement = $db->prepare("INSERT INTO Users (username, password) VALUES(?,?)");
				$statement->bindValue(1, $username);
				$statement->bindValue(2, $password);
				$statement->execute();
				
				$success = UserDAO::signin($username, $password);
			}
			else{
				$success = 'USER_EXIST';
			}

			$db->exec("COMMIT;");
			$db->busyTimeout(500);
			$db->close();
			unset($db);
			return $success;
		}

		public static function chooseGame($idJoueur, $idType){
			$db = new MyDB();
			$db->busyTimeout(500);
			$db->exec("BEGIN;");
			//Récupération d'un partie à joindre.
			$statement = $db->prepare("SELECT Id FROM Session WHERE state != 2 and IdType = ? and ((IdJoueurHost IS NOT NULL and IdJoueurHost != ?) and IdJoueurJoin IS NULL)");
			$statement->bindValue(1, $idType);
			$statement->bindValue(2, $idJoueur);
			$result = $statement->execute();
			$id = $result->fetchArray()[0];

			if(empty($id)){
				// Création d'une nouvelle session si il n'y en a aucune de libre.
				$statement = $db->prepare("INSERT INTO Session (IdJoueurHost, IdType) VALUES(?,?)");
				$statement->bindValue(1, $idJoueur);
				$statement->bindValue(2, $idType);
				$statement->execute();

				//Recupération de l'id de la session créée.
				$statement = $db->prepare("SELECT MAX(Id) FROM Session WHERE IdJoueurHost = ? ORDER BY Id DESC");
				$statement->bindValue(1, $idJoueur);
				$result = $statement->execute();
				$id = $result->fetchArray()[0];
			}
			else{
				//Ajout du joueur à une partie et départ de la partie.
				$statement = $db->prepare("UPDATE Session SET IdJoueurJoin = ?, state = 1 WHERE id = ?");
				$statement->bindValue(1, $idJoueur);
				$statement->bindValue(2, $id);
				$statement->execute();
			}

			$db->exec("COMMIT;");
			$db->busyTimeout(500);
			$db->close();
			unset($db);

			return $id;
		}

		public static function exitGame($idPartie){
			$db = new MyDB();
			$db->busyTimeout(500);
			$db->exec("BEGIN;");
			$statement = $db->prepare("UPDATE Session SET state = 2 WHERE id = ?");
			$statement->bindValue(1, $idPartie);
			$statement->execute();

			$db->exec("COMMIT;");
			$db->busyTimeout(500);
			$db->close();
			unset($db);
		}

		public static function logout($idUser){
			// $db = new MyDB();

			// $statement = $db->prepare("UPDATE Session SET state = 2 WHERE id = ?");
			// $statement->bindValue(1, $idPartie);
			// $statement->execute();
		}
	}