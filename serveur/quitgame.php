<?php
	require_once("action/QuitGameAction.php");

	$action = new QuitGameAction();
	$action->execute();

	echo json_encode($action->result);