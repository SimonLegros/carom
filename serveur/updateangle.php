<?php
	require_once("action/UpdateAngleAction.php");

	$action = new UpdateAngleAction();
	$action->execute();

	echo json_encode($action->result);