CREATE TABLE Users (
	Id integer PRIMARY KEY AUTOINCREMENT,
	username varchar,
	password varchar,
	score integer
);

CREATE TABLE Session (
	Id integer PRIMARY KEY AUTOINCREMENT,
	IdJoueurHost integer,
	IdJoueurJoin integer,
	IdType integer,
	enCours integer,
	angleBaton integer
);

CREATE TABLE Type (
	Id integer PRIMARY KEY AUTOINCREMENT,
	nom varchar
);

CREATE TABLE Pointage (
	Id integer PRIMARY KEY AUTOINCREMENT,
	IdJoueur integer,
	points integer,
	IdSession integer
);

CREATE TABLE Action (
	Id integer PRIMARY KEY AUTOINCREMENT,
	IdSession integer,
	tick integer,
	IdJoueur integer,
	IdParam integer
);

CREATE TABLE Parametres (
	Id integer PRIMARY KEY AUTOINCREMENT,
	force integer,
	angle integer,
	x integer,
	y integer
);

