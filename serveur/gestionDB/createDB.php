<?php
	$db  = new SQLite3('caromdb.db');

	$db->exec("CREATE TABLE Users (
		Id integer PRIMARY KEY AUTOINCREMENT,
		username varchar,
		password varchar,
		niveau integer DEFAULT 0,
		score integer DEFAULT 0
	)");

	$db->exec("CREATE TABLE Session (
		Id integer PRIMARY KEY AUTOINCREMENT,
		IdJoueurHost integer,
		IdJoueurJoin integer,
		IdType integer,
		state integer DEFAULT 0,
		angleBaton integer DEFAULT 0,
		distance integer DEFAULT 0
	)");

	$db->exec("CREATE TABLE Type (
		Id integer PRIMARY KEY AUTOINCREMENT,
		nom varchar
	)");

	$db->exec("CREATE TABLE Pointage (
		Id integer PRIMARY KEY AUTOINCREMENT,
		IdJoueur integer,
		points integer,
		IdSession integer
	)");
	$db->exec("CREATE TABLE Action (
		Id integer PRIMARY KEY AUTOINCREMENT,
		IdSession integer,
		tick integer,
		IdJoueur integer,
		IdParam integer
	)");

	$db->exec("CREATE TABLE Parametres (
		Id integer PRIMARY KEY AUTOINCREMENT,
		force integer,
		angle integer,
		x REAL,
		y REAL
	)");

	// Insertion de users test.
	$db->exec("INSERT INTO Users (username, password) VALUES ('alex', 'abcd')");
	$db->exec("INSERT INTO Users (username, password) VALUES ('test', 'test')");

	// Insertion des types de partie
	$db->exec("INSERT INTO Type (id, nom) VALUES (0, 'Libre')");
	$db->exec("INSERT INTO Type (id, nom) VALUES (1, '1 Bande')");
	$db->exec("INSERT INTO Type (id, nom) VALUES (2, '3 Bandes')");


