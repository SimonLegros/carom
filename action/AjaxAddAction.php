<?php
	require_once("action/CommonAction.php");
	require_once("DAO/GameDAO.php");

	class AjaxAddAction extends CommonAction {

		public $result;

		public function __construct(){
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		public function executeAction(){
			$this->result = "exec";
			if(isset($_SESSION["id"]) && isset($_POST["idPartie"]) && isset($_POST["angle"]) && 
			   isset($_POST["x"]) && isset($_POST["y"]) && isset($_POST["force"])){
				$this->result = GameDAO::addAction($_SESSION["id"], $_POST["idPartie"], $_POST["angle"], $_POST["x"], $_POST["y"], $_POST["force"]);
			}
		}
	}