<?php
	require_once("action/CommonAction.php");
	require_once("DAO/UserDAO.php");

	class IndexAction extends CommonAction {

		public $wrongLogin = false;
		public $lastUsername;
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
			if(!empty($_COOKIE["username"])){
				$this->lastUsername = $_COOKIE["username"];
			}
		}

		protected function executeAction() {
			$result = "";
			if(isset($_GET["action"]) && $_GET["action"] == "login"){
				if (isset($_POST["username-login"]) && isset($_POST["password-login"])) {
					$result = UserDAO::authenticate($_POST["username-login"], $_POST["password-login"]);
				}
				// Si le serveur a retourné un id.
				if (isset($_SESSION["id"])) {
					$_SESSION["username"] = $_POST["username-login"];
					// Conservation du username.
					setcookie("username", $_POST["username-login"], time() + 3600);
					$this->lastUsername = $_COOKIE["username"];
					header("location:mainmenu.php");
					exit;
				}
				// Gestion des erreurs.
				else if($result === "USER_NOT_FOUND"){
					$this->wrongLogin = true;
					$this->status = "Votre user n'est pas valide.";
				}
				else if($result === "PASSWORD_INVALID"){
					$this->wrongLogin = true;
					$this->status = "Votre mot de passe n'est pas valide.";
				}
			}
			else if(isset($_GET["action"]) && $_GET["action"] == "signin"){
				if (isset($_POST["username-signin"]) && isset($_POST["password-signin"])) {
					$result = UserDAO::register($_POST["username-signin"], $_POST["password-signin"]);
					// echo $result;
					if(isset($_SESSION["id"])){
						$_SESSION["username"] = $_POST["username-signin"];
						// Conservation du username.
						setcookie("username", $_POST["username-signin"], time() + 3600);
						$this->lastUsername = $_COOKIE["username"];
						header("location:mainmenu.php");
						exit;
					}
					else{
						$this->wrongLogin = true;
						$this->status = "Nom d'utilisateur déjà existant";
					}
				}
			}
		}
	}