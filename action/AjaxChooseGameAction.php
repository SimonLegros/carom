<?php
	require_once("CommonAction.php");
	require_once("DAO/UserDAO.php");

	class AjaxChooseGameAction extends CommonAction{

		public $result;

		public function __construct(){
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		public function executeAction(){
			if (isset($_SESSION["id"]) && isset($_POST["type-partie"])){
				$this->result = UserDAO::chooseGame($_SESSION["id"], $_POST["type-partie"]);
			}
		}
	}