<?php
	require_once("CommonAction.php");
	require_once("DAO/UserDAO.php");

	class AjaxQuitGameAction extends CommonAction {

		public $result;

		public function __construct(){
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		public function executeAction(){
			if (isset($_SESSION["id"]) && isset($_POST["id-partie"])){
				$this->result = UserDAO::quitGame($_SESSION["id"], $_POST["id-partie"]);
			}
		}
	}