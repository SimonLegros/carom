<?php
	require_once("CommonAction.php");
	require_once("DAO/GameDAO.php");

	class AjaxInfosGameAction extends CommonAction {

		public $result;

		public function __construct(){
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		public function executeAction(){
			if (isset($_POST["idPartie"])){
				$this->result = GameDAO::getInfos($_POST["idPartie"]);
			}
		}
	}

