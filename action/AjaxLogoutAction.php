<?php
	require_once("CommonAction.php");
	require_once("DAO/UserDAO.php");

	class AjaxLogoutAction extends CommonAction {

		public $result;

		public function __construct(){
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		public function executeAction(){
			if (isset($_SESSION["id"])){
				UserDAO::Logout($_SESSION["id"]);
			}
		}
	}