<?php
	require_once("ApiDAO.php");

	class UserDAO {
		public static function authenticate($username, $password){
			$visibility = 0;
			$data = [];
			$data["username"] = $username;
			$data["password"] = $password;

			$result = ApiDAO::callAPI('signin', $data);
			
			if(is_numeric($result)){
				$_SESSION["id"] = $result;
				$visibility = 1;
				$_SESSION["visibility"] = $visibility;
			}

			return $result;
		}

		public static function register($username, $password){
			$visibility = 0;
			$data = [];
			$data["username"] = $username;
			$data["password"] = $password;

			$result = ApiDAO::callAPI('register', $data);

			if(is_numeric($result)){
				$_SESSION["id"] = $result;
				$visibility = 1;
				$_SESSION["visibility"] = $visibility;
			}

			return $result;
		}

		public static function chooseGame($idUser, $idType){
			$data = [];
			$data["id"] = $idUser;
			$data["type-partie"] = $idType;

			$result = ApiDAO::callAPI('choosegame', $data);

			return $result;
		}

		public static function quitGame($idUser, $idPartie){
			$data = [];
			$data["idUser"] = $idUser;
			$data["idPartie"] = $idPartie;

			$result = ApiDAO::callAPI('quitgame', $data);
			return $result;
		}

		public static function Logout($idUser){
			ApiDAO::callAPI('logout', [$idUser]);
		}
	}