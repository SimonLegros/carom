<?php
	require_once("ApiDAO.php");

	class GameDAO{
		public static function getInfos($idPartie){
			$data = [];
			$data["idPartie"] = $idPartie;
			
			$result = ApiDAO::callAPI('info-game', $data);

			return $result;
		}
		
		public static function addAction($idUser, $idPartie, $angle , $x, $y, $force){
			$data = [];
			$data["idUser"] = $idUser;
			$data["idPartie"] = $idPartie;
			$data["angle"] = $angle;
			$data["x"] = $x;
			$data["y"] = $y;
			$data["force"] = $force;

			$result = ApiDAO::callAPI('addaction', $data);
			return $result;
		}

		public static function updateAngleJoueur($angle, $distance, $idPartie){
			$data = [];
			$data["angle"] = $angle;
			$data["distance"] = $distance;
			$data["idPartie"] = $idPartie;

			$result = ApiDAO::callAPI('updateangle', $data);
			return $result;
		}
	}