<?php
	class ApiDAO {
		/**
		 * data = array('key1' => 'value1', 'key2' => 'value2');
		 */
		public static function callAPI($service, array $data) {
			//$apiURL = "http://localhost/carom/serveur/" . $service . ".php";
			$apiURL = "http://10.57.49.6:50040/carom/serveur/" . $service . ".php";

			$options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data)
				)
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($apiURL, false, $context);

			if (strpos($result, "<br") !== false) {
					var_dump($result);
					exit;
				}
				
			return json_decode($result);
		}
	}