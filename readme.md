# Programmeurs
 * Alexandre Desgagné
 * Simon Legros
 * Bryan Lim
 * Jean-François Meehan

====================================================================================================== 
 
#CAROM -> Carambole
Jeu de carambole version 3d en web

======================================================================================================

#Version sur le même ordinateur (carom/Action/DAO/ApiDAO.php)
##Information pertinente
Il faut que la ligne 7 soit active et la ligne 8 en commentaire
En ajoutant le dossier complet sur localhost, il faut démarrer le serveur en allant sur le lien suivant

````
http://localhost/carom/serveur/gestionDB/createDB.php
````

Pour aller au login du jeu :

''''
http://localhost/carom/index.php
''''

======================================================================================================

#Version pour jouer avec un serveur distant (carom/Action/DAO/ApiDAO.php)
##Modification dans Apache
sur c:\travail\apacheExt.conf

Listen VOTRE_IP:VOTRE_PORT
 
<VirtualHost *:VOTRE_PORT>
	ServerName localhost
	DocumentRoot "C:\travail\htdocs82"
</VirtualHost>

<Directory "C:\travail\htdocs82">
    Options Indexes FollowSymLinks ExecCGI
    AllowOverride All
    Order allow,deny
    Allow from all
	Require all granted
</Directory>

##Information pertinente
Il faut que la ligne 8 soit active et la ligne 7 en commentaire
Changer votre adresse IP dans la ligne (si nécessaire)
Il faut démarrer le serveur en allant sur le lien suivant
````
http://IP_SERVEUR/carom/serveur/gestionDB/createDB.php
````

======================================================================================================

Ainsi, la base de données et le serveur sera créés

Une fois le serveur installé, il est possible d'aller sur le login de carom
Une fois arrivé sur ce site, vous pouvez vous connecter ou vous enregistrer
Une fois votre enregistrement effectué, vous êtes dirigé vers le choix de parti

Afin de tester les fonctionnalités, il serait important de vous connecter dans une fenêtre privée avec un autre user

Une fois une partie sélectionnée, le joueurs est redirigé vers la partie. Un lobby d'attente apparaît afin d'attendre la connection d'un
deuxième joueur ayant choisi le même type de partie. Une fois le deuxième joueur connecté, la partie commence.

##Lien git
''''
https://bitbucket.org/SimonLegros/carom
''''

======================================================================================================